;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Paul Roux"
      user-mail-address "paul.roux@phys.ens.fr")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-symbol-font' -- for symbols
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol and press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(add-to-list 'default-frame-alist
             '(ns-transparent-titlebar . t))
(add-to-list 'default-frame-alist
             '(ns-appearance . light))

(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light))
(setq doom-variable-pitch-font (font-spec :family "Iosevka Aile" :size 13 :weight 'semi-light))

(when (> (nth 2 (frame-monitor-geometry)) 1800)
  (setq doom-font (font-spec :family "Fira Code" :size 14 :weight 'semi-light)))
(when (> (nth 2 (frame-monitor-geometry)) 1800)
  (setq doom-variable-pitch-font (font-spec :family "Iosevka Aile" :size 15 :weight 'semi-light)))

;; (use-package! modus-themes)
;; Enable Modus Operandi theme
;;(load-theme 'modus-operandi t)

;; Adjust theme options
;;(setq modus-operandi-theme-bold-constructs t
;;      modus-operandi-theme-bold-parens t
;;      modus-operandi-theme-bold-delim nil
;;      modus-operandi-theme-3d-modeline t
;;      modus-operandi-theme-intense-hl-line t
;;      modus-operandi-theme-section-headings t
;;      modus-operandi-theme-org-blocks 'greyscale
;;      modus-operandi-theme-scale-headings nil)

(doom-themes-visual-bell-config)

;; Theme
(setq doom-theme 'doom-tomorrow-day)

;; Remove highlighting of cursor's line
(global-hl-line-mode -1)

;; adjust some faces to complement the theme
;;(custom-set-faces
  ;; Example: Set font-lock-comment-face to a different color
;;  '(font-lock-comment-face ((t (:foreground "#1e91e6")))))

(use-package! visual-fill-column
  :defer t
  :config
  (setq visual-fill-column-center-text t)
  (setq visual-fill-column-width 92)
  (setq visual-fill-column-center-text t))

  (use-package! golden-ratio)

;; (require 'olivetti)

(global-auto-revert-mode t)

(use-package! simpleclip
  :config (simpleclip-mode 1))

(map! "M-c" #'simpleclip-copy
      "M-v" #'simpleclip-paste
      "M-V" #'yank-from-kill-ring)

(setq shell-file-name (executable-find "bash"))

(setq! dired-omit-files "\\`[.]?#\\|\\`[.][.]?\\'\\|^\\.DS_Store\\'\\|^\\.project\\(?:ile\\)?\\'\\|^\\.\\(?:svn\\|git\\)\\'\\|^\\.ccls-cache\\'\\|^\\.auctex-auto\\'\\|\\(?:\\.js\\)?\\.meta\\'\\|\\.\\(?:elc\\|fls\\|synctex\\.gz\\|bak\\|aux\\|brf\\|toc\\|dvi\\|log\\|fdb_latexmk\\|bcf\\|out\\|run\\.xml\\|o\\|pyo\\|swp\\|class\\)\\'"
       )

(setq! completion-ignored-extensions
      '("\\.elc\\'" ; Example extension to ignore
        "\\.fls\\'"
        "\\.synctex\\.gz\\'"
        "\\.bak\\'"
        "\\.aux\\'"
        "\\.brf\\'"
        "\\.toc\\'"
        "\\.dvi\\'"
        "\\.log\\'"
        "\\.fdb_latexmk\\'"
        "\\.bcf\\'"
        "\\.out\\'"
        "\\.run\\.xml\\'"
        "\\.o\\'"
        "\\.pyo\\'"
        "\\.swp\\'"
        "\\.class\\'"
        "\\.bbl\\'"))

(setq evil-cross-lines t)

(setq mac-option-modifier nil
      mac-command-modifier 'meta)

(setq global-hl-line-modes nil)

(map! :map pdf-view-mode-map
      :leader "DEL" #'pdf-history-goto)

  (use-package! saveplace
    :init (setq save-place-limit 100)
    :config (save-place-mode))

  (use-package! super-save
    :diminish super-save-mode
    :defer 2
    :config
    (setq super-save-auto-save-when-idle t
          super-save-idle-duration 10 ;; after 5 seconds of not typing autosave
          super-save-triggers ;; Functions after which buffers are saved (switching window, for example)
          '(evil-window-next evil-window-prev balance-windows other-window next-buffer previous-buffer)
          super-save-max-buffer-size 10000000)
    (super-save-mode +1))

  ;; After super-save autosaves, wait __ seconds and then clear the buffer. I don't like
  ;; the save message just sitting in the echo area.
  (defun pr-clear-echo-area-timer ()
    (run-at-time "2 sec" nil (lambda () (message " "))))
  (advice-add 'super-save-command :after 'pr-clear-echo-area-timer)

(setq trash-directory "~/trash")
(setq delete-by-moving-to-trash t)

(setq! which-key-use-C-h-commands t)

(map! :leader "w %" #'window-swap-states)

(add-hook! 'org-mode-hook 'org-auto-tangle-mode)
(setq! org-auto-tangle-default "t")

    (defun pr/set-org-fonts ()
      (set-face-attribute 'org-document-title nil :font "Iosevka Aile" :weight 'bold :height 1.5)
      (dolist (face '((org-level-1 . 1.3)
                      (org-level-2 . 1.2)
                      (org-level-3 . 1.1)
                      (org-level-4 . 1.05)
                      (org-level-5 . 1.0)
                      (org-level-6 . 1.0)
                      (org-level-7 . 1.0)
                      (org-level-8 . 1.0)))
        (set-face-attribute (car face) nil :font "Iosevka Aile" :weight 'medium :height (cdr face)))

      ;; Make sure org-indent face is available
      (require 'org-indent)

      (set-face-attribute 'fixed-pitch nil :height 1.0)

      ;; Ensure that anything that should be fixed-pitch in Org files appears that way
      (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
      (set-face-attribute 'org-table nil  :inherit 'fixed-pitch)
      (set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
      (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
      (set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch))
      (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
      (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
      (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
      (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)

      ;; Get rid of the background on column views
      (set-face-attribute 'org-column nil :background nil)
      (set-face-attribute 'org-column-title nil :background nil))

(use-package! org-modern
  :hook (org-mode . org-modern-mode)
  :config
  (setq org-modern-label-border 0.3))

(after! org
  (add-hook! 'org-mode-hook #'pr/set-org-fonts)
  (add-hook! 'org-mode-hook '+org-pretty-mode)
  (add-hook! 'org-mode-hook 'variable-pitch-mode)
  (add-hook! 'org-mode-hook 'org-fragtog-mode)
  (add-hook! 'org-mode-hook (display-line-numbers-mode -1))
  ;;    (add-hook! 'org-mode-hook 'org-auto-tangle-mode)
  (setq! org-ellipsis " ▾"
         org-format-latex-options (plist-put org-format-latex-options :scale 1)
         org-special-ctrl-a/e t
         org-src-fontify-natively t
         org-hide-emphasis-markers t
         org-fontify-quote-and-verse-blocks t
         org-src-tab-acts-natively t
         org-edit-src-content-indentation 2
         org-hide-block-startup nil
         ;;          org-src-preserve-indentation nil
         org-startup-folded 'showall
         org-cycle-separator-lines 1
         org-highlight-latex-and-related '(native entities)
         org-pretty-entities-include-sub-superscripts nil)

  (require 'org-src)
  (add-to-list 'org-src-block-faces '("latex" (:inherit default :extend t)))
  ;; Improve org mode looks
  (setq! org-use-sub-superscripts "{}"
         org-startup-with-inline-images t
         org-startup-with-latex-preview t
         org-image-actual-width '(300))

  ;; Interface between org and the julia repl
  (org-babel-do-load-languages 'org-babel-load-languages '((julia . t)
                                                           (python . t)))
  (add-to-list 'org-babel-load-languages '(julia-vterm . t))
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))

(add-hook! 'org-mode-hook (lambda () (add-hook! 'after-save-hook #'pr/org-babel-tangle-config)))

(setq org-export-with-broken-links t
      org-export-with-smart-quotes t
      org-export-allow-bind-keywords t)

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("org-plain-latex"
                 "\\documentclass{article}
           [NO-DEFAULT-PACKAGES]
           [PACKAGES]
           [EXTRA]"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
  (setq! org-latex-src-block-backend 'engraved))


;; Use latexmk when exporting org->pdf
(setq org-latex-pdf-process (list "latexmk -f -pdf %f"))

(setq! org-file-apps '((auto-mode . emacs)
                       ("\\.pdf\\'" . emacs)))

(use-package! org-ref)

;; small function for inserting a link to an arXiv paper.
(defun insert-arxiv-link (arxiv-ref)
  "Inserts a link to the arXiv paper corresponding to ARXIV-REF."
  (interactive "sEnter arXiv reference: ")
  (let* ((url (format "https://arxiv.org/abs/%s" arxiv-ref))
         (link (format "[[%s][arXiv:%s]]" url arxiv-ref)))
    (insert link)))

(map! :leader
      (:map org-mode-map
      :localleader
      :desc "arXiv link" "l a" #'insert-arxiv-link))

(use-package! ox-reveal)

(map! :leader
      (:map 'org-mode-map
            :localleader
            :desc "Write to src file (tangle)" "w" #'org-babel-tangle))

;; (setq +latex-viewers '(pdf-tools))
(add-hook! 'LaTeX-mode-hook #'outline-minor-mode) ;; lets me fold (sub(sub))sections
(setq! outline-blank-line t)
(add-hook! 'LaTeX-mode-hook (lambda () (TeX-fold-mode 1))) ;; mode for hiding macros like \sections etc
;; (add-hook! 'LaTeX-mode-hook #'outline-hide-sublevels) ;; hide sectioning macros by default
(add-hook! 'LaTeX-mode-hook #'menu-bar--display-line-numbers-mode-visual)
;; my keybindings
(map! :leader
      (:map 'LaTeX-mode-map
      (:prefix ("l" . "LaTeX edition")
                   :desc "fold sections" "s" #'outline-hide-sublevels
                   :desc "unfold all content" "a" #'outline-show-all
                   :desc "unfold subtree" "u" #'outline-show-subtree
                   :desc "hide macros in buffer" "b" #'TeX-fold-buffer
                   :desc "fold-dwim macro under point" "f" #'TeX-fold-dwim)))

(add-hook! 'julia-mode-hook #'julia-vterm-mode)
(defalias 'org-babel-execute:julia 'org-babel-execute:julia-vterm)
(defalias 'org-babel-variable-assignments:julia 'org-babel-variable-assignments:julia-vterm)
(setq! inferior-julia-args "-t auto")
